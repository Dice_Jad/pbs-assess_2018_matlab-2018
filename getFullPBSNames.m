function [fullName] = getFullPBSNames()

fullName.STA        = 'Startability';
fullName.GRAa       = 'Gradeability A';
fullName.GRAb       = 'Gradeability B';
fullName.ACC        = 'Acceleration Capability';
fullName.SRTt       = 'Static Rollover Threshold - Tilt Table';
fullName.SRTtrrcu   = 'Static Rollover Threshold - Tilt Table (rrcu)';
fullName.YDC        = 'Yaw Damping Coefficient - 100 km/h';
fullName.RA         = 'Rearward Amplification';
fullName.HSTO       = 'High-speed Transient Offtracking';
fullName.TASP       = 'Tracking Ability on a Straight Path';
fullName.LSSP       = 'Low-speed Swept Path';
fullName.TS         = 'Tail Swing';
fullName.FS         = 'Frontal Swing';
fullName.MoD        = 'Maximum of Difference';
fullName.DoM        = 'Difference of Maxima';
fullName.STFD       = 'Steer-tyre Friction Demand';
fullName.LSSPu      = 'Low-speed Swept Path - Unladen';
fullName.TSu        = 'Tail Swing - Unladen';
fullName.FSu        = 'Frontal Swing - Unladen';
fullName.MoDu       = 'Maximum of Difference - Unladen';
fullName.DoMu       = 'Difference of Maxima - Unladen';
fullName.STFDu      = 'Steer-tyre Friction Demand - Unladen';

end