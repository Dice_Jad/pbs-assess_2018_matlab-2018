# README
The following readme applies to all files in the TruckSim data extraction folder.

# Tickets

# Future Log (To Do)

# Change Log

[2.0.1] - 2018-02-08

**Fixed**

* A period of changelogs have been omitted. However at this point the post processor is fully functional for 2017.1 

**Note**

* This is where the repository will be forked to a 2018 release edition. 2017.1 may still be refined however 2018 will become the main focus from hereon in.

[2.0.0] - 2017-06-09

**Changed**

* File structure improved

[1.2.0] - 2017-06-08

**Added**

* PBS report date update
* PBS report table formats updated
* Centralised script to execute PBS assessment
* Ability to specify a start and end key to 

**Fixed**

* Code formatting (120 characters wide)

[1.1.1] - 2017-06-02

**Fixed**

* Backwards compatability with Matlab 2015a
* Combination structure variable pass through

[1.1.0] - 2017-05-31

**Added**

* Axle load rating to reports
* Function editCombinationCellRegexpBetweenText
* Function editCombinationCellSplitText
* Function extractTextBetweenParenthesis_CombinationCell


**Fixed**

* Reference points Z coordinates
* Some code neatened up
* Moved all data extraction to getTruckSimSimulatonData
* Variable pass through

[1.0.0] - 2017-05-12

* First iteration of code that is being documented.
